<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,600italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/agentur.css">
	<link rel="stylesheet" href="css/flipclock.css">
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js"></script>
	<script src="js/jqfloat.min.js"></script>
	<script src="js/jquery.rwdImageMaps.js"></script>

	<script type="text/javascript" src="js/agentur.js"></script>

	<!-- Google Analytics OptOut -->
	<script>
		// Set to the same value as the web property used on the site
		var gaProperty = 'UA-64279195-1';
		 
		// Disable tracking if the opt-out cookie exists.
		var disableStr = 'ga-disable-' + gaProperty;
		if (document.cookie.indexOf(disableStr + '=true') > -1) {
		  window[disableStr] = true;
		}
		 
		// Opt-out function
		function gaOptout() {
		  document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
		  window[disableStr] = true;
		}
	</script>

	<!-- Google Analytics -->
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-64279195-1', 'auto');
		ga('set', 'anonymizeIp', true);
		ga('set', 'forceSSL', true);
		ga('send', 'pageview');
	</script>
	

	<title>Wir machen das Web!</title>
</head>
<div id="test">
</div>
	<div class="nav">
		<div class="container">
			<a href="index.html" class="logo"></a>
				<ul>
					<li><a href="faszinationweb.html">Faszination Web</a></li>
					<li class="active"><a href="agentur.php">Agentur</a></li>
					<li><a href="berufe.html">Berufe im Web</a></li>
					<li><a href="videos.html">Videos</a></li>
				</ul>
		</div>
	</div>

		<div class="einleitung" >
			<img id="henry1" src="agenturGrafiken/henry1.png" data-0="left:30%; bottom: 10%" data-400="left:110%;">
			<img id="sprechblaseEinleitung" src="agenturGrafiken/sprechblase.png" data-start="left:48%; opacity:1" data-50="opacity:0; left:57%" >
			<img id="pfeilUnten" src="agenturGrafiken/pfeilUnten.png" data-start="left:46%; opacity:1;" data-80="opacity:0;">
		</div>
		<div class="uebergang">
			<br>
			<h1>Erste Station</h1>
			<h2 class="textUebergang">
				Zunächst muss das Projekt geplant werden.<br>
				Versuche doch mal die folgenden <span class="highlight">PHASEN</span> zu ordnen.
			</h2>
			<img id="henry2" src="agenturGrafiken/henry2.png" data-100-top="left:78%; top:0%;" data-400-top="left:120%;" data--550-top="top:175%;" data--850-top="top:175%; left: 100%;">
		
		</div>
		
		<div id="projektleiter">
			<div id="projektRahmen">
			    <div id="phase">
			        <ul class="phase" id="pfeil1">
			            <li class="phasePfeil" ><span>Marketing</span></li>
			        </ul>
			         <ul class="phase" id="pfeil2">
			            <li class="phasePfeil" ><span>Konzeption</span></li>
			        </ul>
			         <ul class="phase" id="pfeil3">
			            <li class="phasePfeil"><span>Analyse</span></li>
			        </ul>
			         <ul class="phase" id="pfeil4">
			            <li class="phasePfeil"><span>Design</span></li>
			        </ul>
			         <ul class="phase" id="pfeil5">
			            <li class="phasePfeil" ><span>Umsetzung</span></li>
			        </ul>
			
			      
			   	</div>          
			   	<div style="clear:both"></div>
			</div>
		<div id="dragHand">
		</div>

		</div>	
		<div class="hintergrundZwischentext" data-bottom="background-position: center 10%;" data-top-bottom="background-position: center 100%;">
			<h1 class="abstandZwischentext">Aber wer plant das Projekt eigentlich?</h1>
			<h2>Das macht der/die  <span class="highlight">WEB-PROJEKTLEITER/IN.</span></h1>
		</div>


		<div class="einleitung">
			<div id="webprojektleiter" class="sprechblase" data-250-top="opacity:0; left: 63%" data-220-top=" opacity:1" data--60-top=" opacity:0">
				<div class="sprechblasenInhalt" >
					<br><p><span class="highlight">WEB Projektleiter/in</span><br>
					Von der theoretischen Planung bis hin zum konrekten Produktkonzept und der Teamleitung.
					Ziel eines Projektleiters ist es das Ziel vor Augen zu haben und effektiv und effizient das Ziel zu erreichen.</p>
				</div>	
				<a class="buttonProjektleiter" onClick="clickJob('projektleiter')">hier gibt's mehr Infos</a>
			</div>

			<img id="figurProjektleiter" src="agenturGrafiken/projektmanager.png">
			<div id="projektleiterIcons">
				<img class="picon" id="projektleiterIcon1" src="agenturGrafiken/projektleiter_icon1.png">
				<img class="picon" id="projektleiterIcon2" src="agenturGrafiken/projektleiter_icon2.png">
				<img class="picon" id="projektleiterIcon3" src="agenturGrafiken/projektleiter_icon3.png">
				<img class="picon" id="projektleiterIcon4"src="agenturGrafiken/projektleiter_icon4.png">
				<img class="picon" id="projektleiterIcon5" src="agenturGrafiken/projektleiter_icon5.png">
				<img class="picon" id="projektleiterIcon7" src="agenturGrafiken/projektleiter_icon7.png">
				<img class="picon" id="projektleiterIcon8" src="agenturGrafiken/projektleiter_icon8.png">
			</div>
			<img id="henry4" src="agenturGrafiken/henry4.png" data-900-top="left:100%" data-50-top="left:35%" data-430-top="left:35%" data--500-top="left:100%">
			
		</div>	


 <!-- Web Designer -->


		<div class="uebergang">
			<h1>Zweite Station</h1>
			<h2 class="textUebergang">
				Das Projekt ist geplant und die Inhalte stehen fest?<br>Dann stellt sich jetzt die Frage:<br>
				Welche Inhalte sollen <span class="highlight">WO</span> und vor allem <span class="highlight">WIE</span> dargestellt werden ?
			</h2>
			<img id="henry5" src="agenturGrafiken/henry5.png" data-500-top="left:75%; top:0%;" data-600-top="left:120%;" data-150-top="top:100%;" data--400-top="top:100%; left: 100%;">
																
		</div>

		<div class="hintergrundZwischentext" data-bottom="background-position: center 10%;" data-top-bottom="background-position: center 100%;">
			<h1 class="abstandZwischentext">Für die <span class="highlight">GESTALTUNG</span> der Webseite </h1>
			<h2>ist der/die <span class="highlight">WEB Designer/in</span> zuständig!</h1>
		</div>
		
		
		<div class="einleitung">
			<div class="sprechblaseRechts" id="sprechblaseDesigner" data-bottom-top="left:-100%"  data-200-top="left:3%; opacity:1"  data-top-bottom="opacity:0">
				<div class="sprechblasenInhaltRechts">
					<br><p><span class="highlight">WEB Designer/in</span><br>
					Vom einfachen Layout bis hin zur komplizierten Webgestaltung. Der/Die Webdesigner/in kümmert<br> sich um das Aussehen der Webseite.</p>
					<a class="button2" onClick="clickJob('designer')">mehr Infos?</a>
				</div>
			</div>
			<div class="sprechblaseKleinRechts" id="sprechblaseDesignerKlein" data-600-top="left:-50%;" data-550-top="left:3%; opacity:1;" data--100-top="opacity:0">
				<div id="sprechblasenInhaltKleinRechts">
					<p style="color: #9f449b; font-weight: bold;">Klicke auf den Farbfächer und wähle eine Farbe deiner Wahl aus!</p>
				</div>
			</div>
			<img id="figurWebdesigner" src="agenturGrafiken/webdesigner.png">
			<div id="webdesignerArbeitsplatz">
				
				<div class="clock"></div>
				
					<div id="spalteLinks"></div>
					<div id="spalteMitteLinks">
						<div id="spalteKlein1" class="spalteKlein">
						</div>
						<div id="spalteKlein2" class="spalteKlein">
						</div>
						<div id="spalteKlein3" class="spalteKlein">
						</div>
						<div style="clear:both"></div>
					</div>
					<div id="spalteMitteRechts">
						<div id="spalteKlein4" class="spalteKlein">
						</div>
						<div id="spalteKlein5" class="spalteKlein">
						</div>
						<div id="spalteKlein6" class="spalteKlein">
						</div>
						<div style="clear:both"></div>
					</div>
					<div id="spalteRechts"></div>
				</div>
			
	
			<img id="henry6" src="agenturGrafiken/henry6.png" data-300-top="left:-40%" data-250-top="left:15%" data-50-top="left:15%" data--500-top="left:-30%" alt="Farbfaecher" usemap="#farbfaecher">
				<map name="farbfaecher" id="farbfaecher">
		    	 <area id="faecher1" shape="poly" coords="248,124,272,108,320,172,328,255,327,229">
		    	 <area id="faecher2" shape="poly" coords="308,92,336,89,340,107,331,216,328,219">
		    	 <area id="faecher3" shape="poly" coords="341,89,370,93,369,109,331,217">
		    	 <area id="faecher4" shape="poly" coords="375,93,402,103,389,136,339,211,334,213">
		    	 <area id="faecher5" shape="poly" coords="411,110,434,125,415,154,345,210,338,211">
		    	 <area id="faecher6" shape="poly" coords="445,131,461,156,431,180,348,210,345,210">
		    	 <area id="faecher7" shape="poly" coords="467,167,478,193,357,238,349,212">
		      </map>
			
		</div>

<!-- Programmierer -->

	<div class="uebergang">
			<h1>Dritte Station</h1>
			<h2 class="textUebergang">
			<p>Die Website ist inhaltlich und grafisch ausgearbeitet?<br>
				Dann gilt es jetzt, die Website <span class="highlight">TECHNISCH UMZUSETZEN</span>,<br>
				sodass sie im Browser aufrufbar ist.
			</p>
			</h2>
			<img id="henry8" src="agenturGrafiken/henry8.png" data-400-top="left:-2%; top:0%;" data-600-top="left:-20%;" data-150-top="top:100%;" data--400-top="top:100%; left: -40%;">

	</div>
	<div class="hintergrundZwischentext" data-bottom="background-position: center 10%;" data-top-bottom="background-position: center 100%;">
			<h1 class="abstandZwischentext">Für die <span class="highlight">Programmierung</span> der Webseite </h1>
			<h2>ist der/die <span class="highlight">WEB Entwickler/in</span> zuständig!</h1>
		</div>
	<div class="einleitung">
		<div id="schriftTafel"><h2>Klicke zwischen die &lt;div&gt;-Tags um Inhalte in den verschiedenen Flächen zu erzeugen!</h2>
		<img id="henry7" src="agenturGrafiken/henry7.png"></div>
		
		<div id="entwicklerArbeitsplatz">
			<div id="display1" contentEditable="true" style="clear:both;">
				<?php echo htmlentities('<div class="außen">Linke Spalte</div>');
				echo "<br>";
				echo htmlentities('<div class="außen">Rechte Spalte</div>');
				echo "<br>";
				echo htmlentities('<div class="mitteWrapper">');
				echo "<br>";
				echo htmlentities('<div class="mitte"></div>');
				echo "<br>";
				echo htmlentities('<div class="mitte"></div>');
				echo "<br>";
				echo htmlentities('<div class="mitte"></div>');
				echo "<br>";
				echo htmlentities('<div class="mitte"></div>');
				echo "<br>";
				echo htmlentities('<div class="mitte"></div>');
				echo "<br>";
				echo htmlentities('<div class="mitte"></div>');
				echo "<br>";
				echo htmlentities('</div>');

				 ?> <br>

			</div>
			<div id="display2">

			</div>
		</div>
		<div class="sprechblase" id="sprechblaseEntwickler" data-250-top="opacity:0;" data-180-top=" right:10%; opacity:1" data-top-bottom=" opacity:0">
				<div class="sprechblasenInhaltRechts">
					<p><span class="highlight">WEB Entwickler/in</span><br>
					HTML- CSS - Javascript – PHP – MYSQL...
					All das sind Techniken, die Web Entwickler zu Bedienen wissen.</p>
					<a class="button2" onClick="clickJob('entwickler')">mehr Infos?</a>
				</div>
		</div>
		
			<img id="entwickler" src="agenturGrafiken/entwicklerin.png" data-400-top="left:-20%" data-250-top="left:2%" data-100-top="left:2%" data--300-top="left:-20%">

	</div>
	
<!-- Social Media Manager -->

	<div class="uebergang">
			<h1>Vierte Station</h1>
			<h2 class="textUebergang">
			<p>Die Webseite ist <span class="highlight">FERTIG!</span><br>Jetzt beginnt die Zeit der <span class="highlight">VERMARKTUNG</span>
				
			</p>
			</h2>
			<img id="henry9" src="agenturGrafiken/henry2.png" data-500-top="left:75%; top:0%;" data-600-top="left:120%;" data-150-top="top:100%;" data--400-top="top:100%; left: 100%;">
	</div>
	<div class="hintergrundZwischentext" data-bottom="background-position: center 10%;" data-top-bottom="background-position: center 100%;">
			<h1 class="abstandZwischentext">Für die <span class="highlight">KOMMUNIKATION</span> der Webseite auf Social-Media Kanälen</h1>
			<h2>ist der/die <span class="highlight">Social Media Manager/in</span> zuständig!</h1>
		</div>
	<div class="einleitung">
		<img id="socialmediamanager" src="agenturGrafiken/marketingmanager.png">
		<div id="socialmediaArbeitsplatz">
			<div id="socialDisplay">
			</div>
		</div>
		<img id="socialmediaIcons" src="agenturGrafiken/marketing_icons.png">
		<img id="henry11" src="agenturGrafiken/henry8.png" data-700-top="left:100%" data-400-top="left:60%"  data-bottom-top="left:100%">
		<div class="sprechblase" id="sprechblaseSocialMedia" data-700-top="left:100%" data-400-top="left:65%" data-0-top="left:65%" data--500-top="left:100%">
				<div class="sprechblasenInhaltRechts" id="sprechblasenInhaltSocialMedia">
					<p><span class="highlight">Social Media Manager/in</span><br>
					HTML- CSS - Javascript – PHP – MYSQL...
					All das sind Techniken, die Web Entwickler zu Bedienen wissen.</p><br>
					<a class="button2" onClick="clickJob('socialmedia')">mehr Infos?</a>
				</div>
		</div>
	</div>

<!-- Web Analyst -->

	<div class="uebergang">
			<h1>Fünfte Station</h1>
			<h2 class="textUebergang">
			<p>Nachdem die Webseite online ist, muss diese ständig <span class="highlight">ANALYSIERT</span> werden!<br>
			wie <span class="highlight">OFT</span>, von <span class="highlight">WO</span> und <span class="highlight">WANN</span> wird die Webseite abgerufen?</p>
			</h2>
	</div>
	
	
	<div class="hintergrundZwischentext" data-bottom="background-position: center 10%;" data-top-bottom="background-position: center 100%;">
			<h1 class="abstandZwischentext">Für die <span class="highlight">ANALYSE</span> der Webseite </h1>
			<h2>ist der/die<span class="highlight">WEB Analyst/in</span> zuständig!</h1>
	</div>
	
	
	<div class="einleitung">
		<img id="webanalyst" src="agenturGrafiken/webanalyst.png">
		<div id="analystArbeitsplatz">
			<div id="analystDisplay">
			</div>
		</div>
		<img id="bildschirmwand" src="agenturGrafiken/Bildschirmwand.png">
		<img id="henry12" src="agenturGrafiken/henry9.png" data-700-top="left:100%" data-400-top="left:60%"  data-bottom-top="left:100%">
		<div class="sprechblase" id="sprechblaseWebAnalyst" data-700-top="left:100%" data-400-top="left:70%"  data-bottom-top="left:100%">
			<div id="sprechblasenInhaltWebAnalyst">
				<p><span class="highlight">WEB Analyst/in</span><br>
				Die Analyse einer Webseite darf nicht vernachlässigt werden!
				Hierfür gibt es sogar einen eigenen Beruf.</p><br>
				<a class="button2" onClick="clickJob('webanalyst')">mehr Infos?</a>	
			</div>
		</div>
	</div>
		

<!-------------- Footer -------------------------->
		
		<div id="footer">
	<div id="footer_content">
		<div id="footer_logo">
		<a href="index.html"><img src="img/logo_white.png" alt="Wir machen das Web - Logo"></img></a>
		</div>

		<div id="footer-zusatz">
			Berufe im Web erklärt<br/>
			im Rahmen einer Studienarbeit<br/>
			von<br/><br/>
			<span>Anna-Lena Janus<br/>
			Marie Belkenheid<br/>
			Linda Kreiser<br/>
			Svenja Brinschwitz<br/>
			Laura Hailer<br/>
			Melanie Arweiler<br/></span>
		</div>

		<div id="footer_navi">
			<a href="projekt.html">Über das Projekt</a> | 
			<a href="impressum.html">Impressum</a> | 
			<a href="datenschutz.html">Datenschutz</a> 			
		</div>
	</div>
</div>		
		
		
		


	<script src="js/flipclock.min.js"></script>
	<script type="text/javascript" src="parallax/dist/skrollr.min.js"></script>
    <script type="text/javascript">
    var s = skrollr.init();


    </script>
</body>
</html>