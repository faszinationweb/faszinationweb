<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,600italic' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/videos.css">

	<title>Wir machen das Web!</title>
</head>
<body>
	<?php
		$videoNummer =  $_GET["video"];
	?>
	<div class="nav">
			<div class="container">
				<a href="index.html" class="logo"></a>
				<ul>
					<li><a href="faszinationweb.html">Faszination Web</a></li>
					<li><a href="agentur.php">Agentur</a></li>
					<li><a href="berufe.html">Berufe im Web</a></li>
					<li class="active"><a href="videos.html">Videos</a></li>
				</ul>
			</div>
		</div>
	<div id="wrapper">
		<!-- -------------------- VIDEO -------------------- -->	
		<div class="responsive-video">
			<iframe width="1600" height="900" src="https://www.youtube.com/embed/<?php echo $videoNummer; ?>" frameborder="0" allowfullscreen></iframe>
		</div>
	</div>
		<div class="clear"></div>
	<div id="footer">
		<div id="footer_content">
			<div id="footer_logo">
				<a href="index.html"><img src="img/logo.png" alt="Wir machen das Web - Logo"></img></a>
			</div>
			<div id="footer_navi">
				<a href="projekt.html">Über das Projekt</a> | 
				<a href="impressum.html">Impressum</a> | 
				<a href="datenschutz.html">Datenschutz</a> 			
			</div>
		</div>
	</div>

</body>
</html>