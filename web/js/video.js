$(document).ready(function(){
   
	   $("#design").mouseover(function(){
		$("#design").attr('src', 'img/design-mouseover.png');
	});
	    $("#design").mouseout(function(){
	    if ($("#design").attr('src') != "img/design-aktiv.png") { 
	   			$("#design").attr('src', 'img/design.png'); 
			}
		});

	    $("#technik").mouseover(function(){
		$("#technik").attr('src', 'img/technik-mouseover.png');
	});
	    $("#technik").mouseout(function(){
	    	if ($("#technik").attr('src') != "img/technik-aktiv.png") { 
	   			$("#technik").attr('src', 'img/technik.png'); 
			}
	}); 

	     $("#analyse").mouseover(function(){
		$("#analyse").attr('src', 'img/psychologie-mouseover.png');
	});
	    $("#analyse").mouseout(function(){
	    	if ($("#analyse").attr('src') != 'img/psychologie-aktiv.png') { 
	   			$("#analyse").attr('src', 'img/psychologie.png'); 
			}
	}); 

	     $("#marketing").mouseover(function(){
		$("#marketing").attr('src', 'img/marketing-mouseover.png');
	});
	    $("#marketing").mouseout(function(){
	    	if ($("#marketing").attr('src') != "img/marketing-aktiv.png") { 
	   			$("#marketing").attr('src', 'img/marketing.png'); 
			}
	}); 

	     $("#management").mouseover(function(){
		$("#management").attr('src', 'img/management-mouseover.png');
	});
	    $("#management").mouseout(function(){
	    	if ($("#management").attr('src') != "img/management-aktiv.png") { 
	   			$("#management").attr('src', 'img/management.png'); 
			}
	});

	    $("#design").click(function(){
	    	if ($("#design").attr('src') != "img/design-aktiv.png"){
			    $("#design").attr('src', 'img/design-aktiv.png');
				$(".technik").css('background-color', 'black');
				$(".technik").css('opacity', '0.3');
				$(".management").css('background-color', 'black');
				$(".management").css('opacity', '0.3');
				$(".analyse").css('background-color', 'black');
				$(".analyse").css('opacity', '0.3');
				$(".marketing").css('background-color','black');
				$(".marketing").css('opacity', '0.3');
				$(".design").css('background-color', '');
				$(".design").css('opacity', '');
				if ($("#management").attr('src') == "img/management-aktiv.png"){
					$("#management").attr('src', 'img/management.png'); 
				}
				if ($("#technik").attr('src') == "img/technik-aktiv.png"){
					$("#technik").attr('src', 'img/technik.png'); 
				}
				if ($("#analyse").attr('src') == "img/psychologie-aktiv.png"){
					$("#analyse").attr('src', 'img/psychologie.png'); 
				}
				if ($("#marketing").attr('src') == "img/marketing-aktiv.png"){
					$("#marketing").attr('src', 'img/marketing.png'); 
				}
			}
			else {
				$("#design").attr('src', 'img/design.png');
				$(".technik").css("background-color", '');
				$(".technik").css("opacity", '');
				$(".management").css('background-color', '');
				$(".management").css('opacity', '');
				$(".analyse").css('background-color', '');
				$(".analyse").css('opacity', '');
				$(".marketing").css('background-color','');
				$(".marketing").css('opacity', '');
			}
		});


		$("#technik").click(function(){
			if ($("#technik").attr('src') != "img/technik-aktiv.png"){
				$("#technik").attr('src', 'img/technik-aktiv.png');
				$(".design").css('background-color', 'black');
				$(".design").css('opacity', '0.3');
				$(".management").css('background-color', 'black');
				$(".management").css('opacity', '0.3');
				$(".analyse").css('background-color', 'black');
				$(".analyse").css('opacity', '0.3');
				$(".marketing").css('background-color', 'black');
				$(".marketing").css('opacity', '0.3');
				$(".technik").css('background-color', '');
				$(".technik").css('opacity', '');

				if ($("#design").attr('src') == "img/design-aktiv.png"){
					$("#design").attr('src', 'img/design.png'); 
				}
				if ($("#analyse").attr('src') == "img/psychologie-aktiv.png"){
					$("#analyse").attr('src', 'img/psychologie.png'); 
				}
				if ($("#marketing").attr('src') == "img/marketing-aktiv.png"){
					$("#marketing").attr('src', 'img/marketing.png'); 
				}
				if ($("#management").attr('src') == "img/management-aktiv.png"){
					$("#management").attr('src', 'img/management.png'); 
				}
			}
			else
			{
				$("#technik").attr('src', 'img/technik.png');
				$(".design").css("background-color", '');
				$(".design").css("opacity", '');
				$(".management").css('background-color', '');
				$(".management").css('opacity', '');
				$(".analyse").css('background-color', '');
				$(".analyse").css('opacity', '');
				$(".marketing").css('background-color','');
				$(".marketing").css('opacity', '');
			}
		});

		$("#management").click(function(){
			if ($("#management").attr('src') != "img/management-aktiv.png"){
				$("#management").attr('src', 'img/management-aktiv.png');	
				$(".design").css('background-color', 'black');
				$(".design").css('opacity', '0.3');
				$(".technik").css('background-color', 'black');
				$(".technik").css('opacity', '0.3');
				$(".analyse").css('background-color', 'black');
				$(".analyse").css('opacity', '0.3');
				$(".marketing").css('background-color', 'black');
				$(".marketing").css('opacity', '0.3');
				$(".management").css('background-color', '');
				$(".management").css('opacity', '');

				if ($("#design").attr('src') == "img/design-aktiv.png"){
					$("#design").attr('src', 'img/design.png'); 
				}
				if ($("#technik").attr('src') == "img/technik-aktiv.png"){
					$("#technik").attr('src', 'img/technik.png'); 
				}
				if ($("#analyse").attr('src') == "img/psychologie-aktiv.png"){
					$("#analyse").attr('src', 'img/psychologie.png'); 
				}
				if ($("#marketing").attr('src') == "img/marketing-aktiv.png"){
					$("#marketing").attr('src', 'img/marketing.png'); 
				}
			}
			else {
				$("#management").attr('src', 'img/management.png');
				$(".design").css("background-color", '');
				$(".design").css("opacity", '');
				$(".technik").css('background-color', '');
				$(".technik").css('opacity', '');
				$(".analyse").css('background-color', '');
				$(".analyse").css('opacity', '');
				$(".marketing").css('background-color','');
				$(".marketing").css('opacity', '');
			}
		});

		$("#analyse").click(function(){
			if ($("#analyse").attr('src') != "img/psychologie-aktiv.png")
			{
				$("#analyse").attr('src', 'img/psychologie-aktiv.png');
				$(".design").css('background-color', 'black');
				$(".design").css('opacity', '0.3');
				$(".technik").css('background-color', 'black');
				$(".technik").css('opacity', '0.3');
				$(".management").css('background-color', 'black');
				$(".management").css('opacity', '0.3');
				$(".marketing").css('background-color', 'black');
				$(".marketing").css('opacity', '0.3');
				$(".analyse").css('background-color', '');
				$(".analyse").css('opacity', '');

				if ($("#management").attr('src') == "img/management-aktiv.png"){
					$("#management").attr('src', 'img/management.png'); 
				}
				if ($("#technik").attr('src') == "img/technik-aktiv.png"){
					$("#technik").attr('src', 'img/technik.png'); 
				}
				if ($("#design").attr('src') == "img/design-aktiv.png"){
					$("#design").attr('src', 'img/design.png'); 
				}
				if ($("#marketing").attr('src') == "img/marketing-aktiv.png"){
					$("#marketing").attr('src', 'img/marketing.png'); 
				}
			}
			else {
				$("#analyse").attr('src', 'img/psychologie.png');
				$(".design").css("background-color", '');
				$(".design").css("opacity", '');
				$(".technik").css('background-color', '');
				$(".technik").css('opacity', '');
				$(".management").css('background-color', '');
				$(".management").css('opacity', '');
				$(".marketing").css('background-color','');
				$(".marketing").css('opacity', '');				
			}
		});


		$("#marketing").click(function(){
			if ($("#marketing").attr('src') != "img/marketing-aktiv.png")
			{									
				$("#marketing").attr('src', 'img/marketing-aktiv.png');
				$(".design").css('background-color', 'black');
				$(".design").css('opacity', '0.3');
				$(".technik").css('background-color', 'black');
				$(".technik").css('opacity', '0.3');
				$(".analyse").css('background-color', 'black');
				$(".analyse").css('opacity', '0.3');
				$(".management").css('background-color', 'black');
				$(".management").css('opacity', '0.3');
				$(".marketing").css('background-color', '');
				$(".marketing").css('opacity', '');

				if ($("#management").attr('src') == "img/management-aktiv.png"){
					$("#management").attr('src', 'img/management.png'); 
				}
				if ($("#technik").attr('src') == "img/technik-aktiv.png"){
					$("#technik").attr('src', 'img/technik.png'); 
				}
				if ($("#design").attr('src') == "img/design-aktiv.png"){
					$("#design").attr('src', 'img/design.png'); 
				}
				if ($("#analyse").attr('src') == "img/psychologie-aktiv.png"){
					$("#analyse").attr('src', 'img/psychologie.png'); 
				}
			}
			else {
				$("#marketing").attr('src', 'img/marketing.png');
				$(".design").css("background-color", '');
				$(".design").css("opacity", '');
				$(".technik").css('background-color', '');
				$(".technik").css('opacity', '');
				$(".management").css('background-color', '');
				$(".management").css('opacity', '');
				$(".analyse").css('background-color','');
				$(".analyse").css('opacity', '');	
			}
	});		




});

  