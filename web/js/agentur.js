

$(document).ready(function(){
   
   var reihenfolge = ['pfeil2', 'pfeil4', 'pfeil5', 'pfeil1', 'pfeil3'];

  $( "#phase" ).sortable({
  
  		start: function( event, ui ) {
  			ui.item.zIndex(2000);
  		},
  		update: function( event, ui ) {
  			var richtigeReihenfolge = true;
  			var i = 4;
  			var aktuelleReihenfolge = [];
  			var s =0;
  			$("#phase").children().each(function(){
  				aktuelleReihenfolge[s] = $(this).attr('id');
  				s++;
  			});

  			$("#phase").children().each(function(){
  				//$(this).children().css('position', 'relative');
  				$(this).children().css('z-index', 1000 + i);
  				i--;
  			});

  			
  			for ( i = 0; i <= reihenfolge.length; i++ ){
  				if ( reihenfolge[i] != aktuelleReihenfolge[i]){
					richtigeReihenfolge = false;
  				}
  			} 
  			if (richtigeReihenfolge == true){
  				goHenry();
  			}
  				
  		}		
	});

function goHenry(){
	$("#henry2").attr('src', "agenturGrafiken/henry3.png");

}

  $('.picon').each(function(){
      $(this).jqFloat({
      width: 30,
      height: 30,
      speed: 1500
    });
  });


 
  var clock = $('.clock').FlipClock({
  clockFace: 'TwentyFourHourClock'
});

  var farbdivs = ['spalteKlein1', 'spalteKlein2', 'spalteKlein3', 'spalteKlein4', 'spalteKlein5', 'spalteKlein6', 'spalteLinks', 'spalteRechts'];

$("#faecher1").click(function(){
    var divElement = farbdivs[Math.floor(Math.random() * farbdivs.length)];
    $("#" + divElement).css('background-color', '#ff763a');
});
$("#faecher2").click(function(){
    var divElement = farbdivs[Math.floor(Math.random() * farbdivs.length)];
    $("#" + divElement).css('background-color', '#ffad47');
});
$("#faecher3").click(function(){
    var divElement = farbdivs[Math.floor(Math.random() * farbdivs.length)];
    $("#" + divElement).css('background-color', '#ffdf3a');
});
$("#faecher4").click(function(){
    var divElement = farbdivs[Math.floor(Math.random() * farbdivs.length)];
    $("#" + divElement).css('background-color', '#54a620');
});
$("#faecher5").click(function(){
    var divElement = farbdivs[Math.floor(Math.random() * farbdivs.length)];
    $("#" + divElement).css('background-color', '#2c68b3');
});
$("#faecher6").click(function(){
    var divElement = farbdivs[Math.floor(Math.random() * farbdivs.length)];
    $("#" + divElement).css('background-color', '#c246bb');
});
$("#faecher7").click(function(){
    var divElement = farbdivs[Math.floor(Math.random() * farbdivs.length)];
    $("#" + divElement).css('background-color', '#c73084');
});

  var display1 = $('#display1');
  $("#display2").html($.parseHTML(display1.text()));
  
    var display2 = $('#display2');
    $("#socialDisplay").html(display2.html());

   $('#webdesignerArbeitsplatz').bind("DOMSubtreeModified",function(){

      $(".außen").first().css('background-color', $('#spalteLinks').css('background-color') );
      $(".außen:eq(1)").css('background-color', $('#spalteRechts').css('background-color') );
      $(".mitte:eq(0)").css('background-color', $('#spalteKlein1').css('background-color') );
      $(".mitte:eq(2)").css('background-color', $('#spalteKlein2').css('background-color') );
      $(".mitte:eq(4)").css('background-color', $('#spalteKlein3').css('background-color') );
      $(".mitte:eq(1)").css('background-color', $('#spalteKlein4').css('background-color') );
      $(".mitte:eq(3)").css('background-color', $('#spalteKlein5').css('background-color') );
      $(".mitte:eq(5)").css('background-color', $('#spalteKlein6').css('background-color') );

  });

  display1.bind("DOMSubtreeModified",function(){
    var display1 = $('#display1');
    var display2 = $('#display2');
    $("#display2").html($.parseHTML(display1.text()));
    $("#socialDisplay").html(display2.html());

  });

  //Responsive Image Map
    $('#henry6[usemap]').rwdImageMaps();

  // Social Media Manager Display

  $("#display2").bind("DOMSubtreeModified",function(){
    var display2 = $('#display2');
    $("#socialDisplay").html(display2.html());

  });

});

// Go to Job
function clickJob(beruf){

     $("body").load( "berufe.html", function() {
              var job = $("body").find("#job_" + beruf);
              var details = job.find('.details');
              $('.details').slideUp();
              details.slideToggle(function() {
                var top = job.position().top - 150;
                $('html,body').animate({
                  scrollTop: top
                });
              });
     });
  }

