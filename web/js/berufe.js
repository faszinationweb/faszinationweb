$(document).ready(function(){ 

	// Aufklappfunktion der Details eines Jobs
	$('.job .arrow-down').click(function(e) {
		e.preventDefault();
		var job = $(this).closest('.job');
		var details = job.find('.details');
		$('.details').not(details).slideUp();
		details.slideToggle(function() {
			var top = job.position().top - 150;
			$('html,body').animate({
				scrollTop: top
			});
		});
	});

	// Videos auf gesamter Screenhöhe anzeigen
	$('.video iframe').each(function(){
		var height = $(window).height() - 80;
		$(this).height(height);
	});

	// Header mit Sprungmarke richtig positionieren
	$('.categories .category, .navi a, .arrow-down').click(function(e){
		e.preventDefault();
		var anchor = $(this).attr('href').replace('#', '');
		var top = $('.group.' + anchor).position().top - 150;
		$('html,body').animate({
			scrollTop: top
		});
	});

	// Einblenden der Kategorienavigation
	var firstGroupTop = $('.group:first').position().top - 160;
	$(window).scroll(function(){
		var top = $(window).scrollTop();
		if(top >= firstGroupTop) {
			$('.navi').slideDown(300);
		} else {
			$('.navi').slideUp(300);
		}
	});

});